import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class DbConnection {
	
	static Connection c = null;
	
	static
	{
	    if(c==null)
	    {
		try {
		 Class.forName("org.sqlite.JDBC");
	        c = DriverManager.getConnection("jdbc:sqlite:vms.db");
	        c.setAutoCommit(true);
		} catch ( Exception e ) {
	        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	        System.exit(0);
	      }
	    }
	}
	
	
	public static void main(String[] a)
	{

		Statement stmt = null;
    try {
        
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery( "SELECT name FROM sqlite_master WHERE type='table'" );
        while ( rs.next() ) {
          System.out.println(rs.getString(1));
        }
                
        rs.close();
        stmt.close();
        c.close();
      } catch ( Exception e ) {
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        e.printStackTrace();
        System.exit(0);
      }
      System.out.println("Opened database successfully");
      
    
	
	
	}
	Map m = new HashMap();
}
