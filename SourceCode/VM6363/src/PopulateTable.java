

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;


@WebServlet("/PopulateTable")
public class PopulateTable extends HttpServlet {
 private static final long serialVersionUID = 1L;

    public PopulateTable() {
       
    }
 
 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
  ArrayList<Request> RequestList = new ArrayList<Request>();
  ArrayList<Request> RequestList1 = new ArrayList<Request>();
  String to = request.getParameter("to");
  String from = request.getParameter("from");
  System.out.println(from+" : "+to);
  RequestList=FetchData.getAllREQUESTs(from,to);
  for(Request req: RequestList){
	  Statement statement1;
	try {
		statement1 = DbConnection.c.createStatement();
		
		int user_id = req.getUser_id();
		String sql1 = "select first_nm from EMPLOYEE where user_id='"+user_id+"'";
	    ResultSet rs1 = statement1.executeQuery(sql1);
	    req.setUser_name(rs1.getString(1));
	    
		String type_cd = req.getType_cd();
		String sql2 = "select description from TIME_OFFS where type_cd='"+type_cd+"'";
	    ResultSet rs2 = statement1.executeQuery(sql2);
	    req.setLeave_type(rs2.getString(1));
	    
	    RequestList1.add(req);
	} 
	catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
  }
  Gson gson = new Gson();
  JsonElement element = gson.toJsonTree(RequestList1, new TypeToken<List<Request>>() {}.getType());

  JsonArray jsonArray = element.getAsJsonArray();
  response.setContentType("application/json");
  response.getWriter().print(jsonArray);
  
 }

 
 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
 }

}
