package emailnotification;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotification {

    private static Properties serverProps;
    private static Session sessionInfo;
    private static MimeMessage mailInfo;
    private static String msgSubject;
    private static String msgBody;
    private static String sysEmail;
    private static String sysPass;
    private static InternetAddress[] sendToAddresses;

    
    /**
     * Method that sets Up email message parts. The system email address an
     * password are hard coded.  The system email address is set up as a google
     * address with low security setting.
     * @param pMsgSubj - Subject of Email
     * @param pMsgBody - The HTML message body.
     * @param lstSendToAddress - A comma delimited string.
     * @return True if message(s) successfully sent.
     */
    public boolean setUpEmailMsg(String pMsgSubj, String pMsgBody,
             String lstSendToAddress) {
        try {
            msgSubject = pMsgSubj;
            msgBody = pMsgBody;
            sysEmail = "vacationplanner6363";
            sysPass = "vplanner6363!";
            sendToAddresses = InternetAddress.parse(lstSendToAddress);
            generateAndSendEmail();
        } catch (AddressException ex) {
            Logger.getLogger(EmailNotification.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
/**
 * Private method to initialize session and send email(s).
 */
    private static void generateAndSendEmail() {

        try {
            //Server Properties using port 587 - to ensure mail is sent.
            serverProps = System.getProperties();
            serverProps.put("mail.smtp.port", "587");
            serverProps.put("mail.smtp.auth", "true");
            serverProps.put("mail.smtp.starttls.enable", "true");
            
            System.out.println("Get Session");     
            //set up email subject body list of addresses
            sessionInfo = Session.getDefaultInstance(serverProps, null);
            mailInfo = new MimeMessage(sessionInfo);
            mailInfo.addRecipients(Message.RecipientType.TO, sendToAddresses);
            mailInfo.setSubject(msgSubject);
            String emailBody = msgBody;
            mailInfo.setContent(emailBody, "text/html");
            
            //use the system email address and password
            
            System.out.println("Set up transport");            
            Transport transport = sessionInfo.getTransport("smtp");
            transport.connect("smtp.gmail.com", sysEmail, sysPass);
            transport.sendMessage(mailInfo, mailInfo.getAllRecipients());
            transport.close();
        } catch (MessagingException ex) {
            Logger.getLogger(EmailNotification.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Message sent!");     
    }

}
