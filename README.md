GITlab repo for Vacation Manager
# Vacation Manager
A cloud-based web application to manage leaves at work place.
Easy to navigate and a simple design with no learning curve.
No intstallation needed. 

_Link **to** the UI mockups_
https://app.moqups.com/csolanky/TSrY9daEGJ/view

# This is VMS logo!!
![GitHub Logo](/images/vms_logo.png)

[Link to VMS webpage ON EC2] (http://54.68.106.127:8080/VM6363 "VMS Homepage")

## Development Environment
1. Java
2. AWS

## Screenshots
Coming soon...

## Release History
1. VMS 1.0 
    * Work in Progress
    * Interim presentation in class
    * Tentative Beta Release 11/23/2016

###### GO TEAM BRAVO :+1:
###### Agile6363@UNO Fall 2016




