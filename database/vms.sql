BEGIN TRANSACTION;
CREATE TABLE "testimonials" (
	`id`	INTEGER NOT NULL,
	`description`	TEXT,
	PRIMARY KEY(`id`)
);
INSERT INTO `testimonials` (id,description) VALUES (1,'Excellent Product!!
~ABC Inc.'),
 (2,'Glad we found it. Totally recommend.
~XYZ Corp.'),
 (3,'Great, user-friendly application. No learning curve.
~Non-profit, New Orleans');
CREATE TABLE "UserCredentials" (
	`user_id`	INTEGER NOT NULL UNIQUE,
	`username`	TEXT NOT NULL,
	`password`	TEXT NOT NULL,
	PRIMARY KEY(`user_id`)
);
INSERT INTO `UserCredentials` (user_id,username,password) VALUES (1,'shyam','shyam'),
 (2,'rahul','rahul'),
 (3,'anusha','anusha'),
 (4,'tosin','tosin'),
 (5,'chhavi','chhavi'),
 (6,'aditya','aditya'),
 (7,'zibran','zibran');
CREATE TABLE "TIME_OFFS" (
	`type_cd`	INTEGER NOT NULL,
	`description`	TEXT NOT NULL,
	PRIMARY KEY(`type_cd`)
);
INSERT INTO `TIME_OFFS` (type_cd,description) VALUES (1,'PTO'),
 (2,'Sick'),
 (3,'Unpaid'),
 (4,'Training'),
 (5,'Remote');
CREATE TABLE "REQUEST" (
	`request_id`	INTEGER NOT NULL,
	`user_id`	INTEGER NOT NULL,
	`comments`	TEXT,
	`request_dt`	REAL,
	`request_status`	TEXT,
	`hours_requested`	INTEGER,
	`start_dt`	TEXT,
	`end_dt`	TEXT,
	`type_cd`	INTEGER,
	`approved_by`	INTEGER,
	PRIMARY KEY(`request_id`,`user_id`),
	FOREIGN KEY(`user_id`) REFERENCES `EMPLOYEE`(`user_id`)
);
INSERT INTO `REQUEST` (request_id,user_id,comments,request_dt,request_status,hours_requested,start_dt,end_dt,type_cd,approved_by) VALUES (1,1,'Decompress Time!','2016-10-11','pending',16,'2016-11-14','2016-11-15',1,NULL),
 (2,1,'Bahamas!!','2016-10-15','approved',16,'2016-10-12','2016-10-16',1,2),
 (3,1,'Migraine','2016-05-15','approved',8,'2016-05-15','2016-05-16',2,2),
 (4,1,'Scrum Master','2016-10-01','approved',32,'2016-11-14','2016-11-17',4,2),
 (5,1,'AC guy coming','2016-07-11','approved',8,'2016-07-12','2016-07-13',5,2),
 (6,1,'Beach Calling...','2016-06-13','rejected',16,'2016-06-20','2016-06-21',1,2),
 (7,1,'Just Chill!','2016-04-04','rejected',8,'2016-04-16','2016-04-17',1,2),
 (8,1,'Vacation','2016-08-08','approved',40,'2016-08-15','2016-08-19',1,2),
 (9,3,'Fever','2016-04-04','approved',16,'2016-04-04','2016-04-05',2,2),
 (10,3,'Vacation','2016-06-13','approved',8,'2016-06-16','2016-06-16',1,2),
 (11,4,'Florida trip','2016-10-11','approved',32,'2016-10-11','2016-10-14',1,2),
 (12,5,'Disney World!!','2016-10-05','approved',40,'2016-10-17','2016-10-21',1,2),
 (13,5,'Vacation','2016-02-03','rejected',16,'2016-03-03','2016-03-04',1,2),
 (14,6,'Agile Training','2016-05-05','approved',40,'2016-06-20','2016-06-24',4,2),
 (15,5,'Vacation','2016-09-09','approved',40,'2016-09-12','2016-09-16',1,2),
 (16,5,'Relax!','2016-11-10','pending',16,'2016-11-14','2016-11-15',1,''),
 (17,5,'Sick','2016-06-06','approved',16,'2016-06-06','2016-06-07',2,2),
 (18,5,'Work from home','2016-10-03','approved',16,'2016-10-03','2016-10-04',5,2),
 (19,5,'Cancun trip!','2016-05-05','approved',40,'2016-06-06','2016-06-10',1,2),
 (20,5,'WFH!','2016-07-07','approved',8,'2016-07-07','2016-07-08',5,2),
 (21,8,'Sick','2016-08-08','approved',16,'2016-08-08','2016-08-09',2,2);
CREATE TABLE "ORG_HOLIDAYS" (
	`org_id`	INTEGER,
	`holiday_id`	INTEGER,
	`holiday_dt`	TEXT,
	`holiday_desc`	TEXT,
	PRIMARY KEY(`holiday_id`)
);
INSERT INTO `ORG_HOLIDAYS` (org_id,holiday_id,holiday_dt,holiday_desc) VALUES (1,1,'2016-11-11','Veterans Day'),
 (1,2,'2016-11-24','Thanksgiving Day'),
 (1,3,'2016-12-25','Christmas'),
 (1,4,'2016-12-30','New Year''s Eve'),
 (1,5,'2016-01-01','New Year''s Day');
CREATE TABLE `ORG_BLOCKDAYS` ( `org_id` INTEGER, 
`blockday_id` INTEGER, `blockday_dt` TEXT, 
`blockday_desc` TEXT, PRIMARY KEY(`blockday_id`) );
INSERT INTO `ORG_BLOCKDAYS` (org_id,blockday_id,blockday_dt,blockday_desc) VALUES (1,1,'2016-12-06','Christmas Party'),
 (1,2,'2016-12-07','Education Workshop');
CREATE TABLE "ORGANIZATION" (
	`org_id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL,
	`city`	TEXT,
	PRIMARY KEY(`org_id`)
);
INSERT INTO `ORGANIZATION` (org_id,name,city) VALUES (1,'UNO','New Orleans');
CREATE TABLE "EMPLOYEE_GROUPS" (
	`group_id`	INTEGER NOT NULL,
	`description`	TEXT NOT NULL,
	PRIMARY KEY(`group_id`)
);
INSERT INTO `EMPLOYEE_GROUPS` (group_id,description) VALUES (1,'Team Alpha'),
 (2,'Team Beta'),
 (3,'Team Delta');
CREATE TABLE "EMPLOYEE_CATEGORY" (
	`category_id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL,
	`user_id`	INTEGER,
	PRIMARY KEY(`category_id`)
);
INSERT INTO `EMPLOYEE_CATEGORY` (category_id,name,user_id) VALUES (1,'admin',3),
 (2,'manager',2),
 (3,'employee',1);
CREATE TABLE "EMPLOYEE" (
	`user_id`	INTEGER NOT NULL,
	`first_nm`	TEXT NOT NULL,
	`middle_nm`	TEXT,
	`last_nm`	TEXT NOT NULL,
	`hire_dt`	REAL NOT NULL,
	`active`	INTEGER NOT NULL DEFAULT 0,
	`manager_id`	INTEGER,
	`employee_category`	INTEGER NOT NULL,
	`org_id`	INTEGER,
	`group_id`	INTEGER,
	`email`	TEXT,
	`address`	TEXT,
	`cell_phone`	TEXT,
	`gender`	TEXT,
	PRIMARY KEY(`user_id`),
	FOREIGN KEY(`employee_category`) REFERENCES `category_id`
);
INSERT INTO `EMPLOYEE` (user_id,first_nm,middle_nm,last_nm,hire_dt,active,manager_id,employee_category,org_id,group_id,email,address,cell_phone,gender) VALUES (1,'Shyam','','Bagineni','',1,2,3,1,1,'skbagine@uno.edu','456 Lakeshore Dr., NOLA, 70124','888.888.8888','M'),
 (2,'Rahul','','Reddy','',1,0,2,1,1,'rreddy@uno.edu','123 ABC st., NOLA, 70124','504.333.3333','M'),
 (3,'Anusha','','Yella','',1,2,3,1,1,'ayella@uno.edu','345 Rue Royal, Metairie, LA 70001','718.666.4532','F'),
 (4,'Tosin','','King','',1,2,3,1,2,'tking@uno.edu','32 Edenborn dr, Biloxi, MS 31441','561.342.1414','F'),
 (5,'Chhavi','','Solanky','',1,2,3,1,2,'csolanky@uno.edu','414 Rue Laurent, Metairie, LA 70002','504.723.7865','F'),
 (6,'Aditya','','Madhula','',1,2,3,1,2,'nmadula@uno.edu','32 Airline Hwy, Metairie, LA 70003','504.563.4343','M'),
 (7,'Minhaz','','Zibran','',1,2,3,1,2,'mzibran@uno.edu','CS dept, UNo, New Orleans, LA 70124','504.453.4563','M'),
 (8,'Jane','','Doe','',1,2,3,1,1,'jdoe@uno.edu','345 ABC st. NOLA 70124','547.345.5477','F');
CREATE TABLE "BALANCE" (
	`user_id`	INTEGER NOT NULL,
	`begin_balance`	INTEGER,
	`taken_for_year`	INTEGER,
	`accrued_for_year`	INTEGER,
	`available`	INTEGER,
	PRIMARY KEY(`user_id`),
	FOREIGN KEY(`user_id`) REFERENCES `EMPLOYEE`(`USER_id`)
);
INSERT INTO `BALANCE` (user_id,begin_balance,taken_for_year,accrued_for_year,available) VALUES (1,5,8,15,11),
 (3,5,3,15,17),
 (4,5,4,15,16),
 (5,5,16,15,4),
 (6,5,10,15,10),
 (8,5,2,15,18);
COMMIT;
